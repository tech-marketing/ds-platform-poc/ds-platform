from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

engine = create_engine(URL(
    account = '',
    user = '',
    password = '',
    database = 'SNOWFLAKE_SAMPLE_DATA'
))

connection = engine.connect()
print("connecting to DB...")
try:
    result = connection.execute('select current_version()').fetchone()
    print(f"Query results: {result[0]}")

finally:
    connection.close()
    engine.dispose()


print(engine)
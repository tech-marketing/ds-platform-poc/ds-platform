import logging
import sys

from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from typing import Dict, List


class SnowflakeManager:

    def __init__(self):
        self.engine = create_engine(
            URL(
                user='',
                account='',
                password='',
                database='SNOWFLAKE_SAMPLE_DATA'
))

    def connect(self):
        connection = self.engine.connect()
        logging.info("Connection established...")
        try:
            result = connection.execute('select current_version()').fetchone()
            print(f"Query results: {result[0]}")

        finally:
            connection.close()
            self.engine.dispose()


if __name__ == "__main__":
    snowflake_manager = SnowflakeManager()
    snowflake_manager.connect()
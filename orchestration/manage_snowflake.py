"""
Connects to Snowflake instance and execute testing commands

Author: William Arias

GitLab Handle: @warias


"""

import logging
import hydra
import sys

from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from hydra.utils import instantiate
from omegaconf import DictConfig

logging.basicConfig(stream=sys.stdout, level=20)


class SnowflakeManager:

    def __init__(self, config_vars: DictConfig):
        self.engine = create_engine(
            URL(
                user=config_vars["SNOWFLAKE_USER"],
                account=config_vars["SNOWFLAKE_ACCOUNT"],
                password=config_vars["SNOWFLAKE_PASSWORD"],
                database=config_vars['DATABASE_NAME']
            )
        )

    def connect(self):

        connection = self.engine.connect()
        logging.info("Connection established...")
        try:
            result = connection.execute('select current_version()').fetchone()
            print(f"Query results: {result[0]}")

        finally:
            connection.close()
            self.engine.dispose()


@hydra.main(version_base=None, config_path="../conf", config_name="config")
def app(cfg: DictConfig) -> None:
    snowflake_manager = instantiate(cfg.db)
    snowflake_manager.connect()


if __name__ == "__main__":
    app()
    